# EvoBot

A lightweight (Alpine) Docker image of [EvoBot](https://github.com/eritislami/evobot) with AMD64 and ARM64 support.

## Run

An example of `config.json` can be found [here](https://raw.githubusercontent.com/eritislami/evobot/master/config.json.example).

`docker run -it -v "${PWD}/config.json:/home/evobot/config.json" cl00e9ment/evobot`

For more info, see the [EvoBot README](https://github.com/eritislami/evobot#readme).

## Resources

- [source code](https://github.com/eritislami/evobot)
- [build chain](https://gitlab.com/Cl00e9ment/evobot)
- [DockerHub page](https://hub.docker.com/r/cl00e9ment/evobot)
