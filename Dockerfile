FROM alpine AS sources
RUN wget https://github.com/eritislami/evobot/archive/refs/heads/master.zip
RUN unzip master.zip && rm -r master.zip
WORKDIR evobot-master
RUN while read file; do rm -rf "$file"; done < .dockerignore
RUN rm .dockerignore Dockerfile

FROM node:14-alpine AS builder
RUN apk add g++ gcc make musl-dev python3
COPY --from=sources /evobot-master/package*.json ./
RUN npm install

FROM node:14-alpine
ENV USER=evobot
RUN addgroup -S ${USER} && adduser -h /home/evobot -S -G ${USER} ${USER}
USER ${USER}
WORKDIR /home/evobot
COPY --chown=${USER}:${USER} --from=builder node_modules node_modules
COPY --chown=${USER}:${USER} --from=sources /evobot-master .
CMD node index.js
